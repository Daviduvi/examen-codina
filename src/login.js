import React, {useState} from "react";
import { useHistory } from "react-router-dom";

import meme from './assets/meme.png'
import isaac from './assets/isaac.png'


function Login() {

    const history = useHistory();

    const routeChange = () => {
        let path = '/home';
        history.push(path);
    }

    let [email, setState] = useState();
    let [password, setPasswordState] = useState();

    let handleChange = (e) => {
        setState({email: e.target.value})
      }

    let handlePasswordChange = (e) => {
        setPasswordState({password: e.target.value})
    }

        return (
            <>
            <form>

                <h3>PALEXIA</h3>

                <div className="form-group">
                    <label>Email</label>
                    <input type="email" className="form-control" onChange={handleChange} placeholder="Enter email" />
                </div>

                <div className="form-group">
                    <label>Password</label>
                    <input type="password" className="form-control" onChange={handlePasswordChange} placeholder="Enter password" />
                </div>

                
                <button type="submit" className="btn btn-dark btn-lg btn-block" disabled={!email || !password} onClick={routeChange}>Login</button>
            </form>
            <img id="dragoBetis" src={meme} alt="" />
            <img id="isaac" src={isaac} alt="Vosaltres en aquest exàmen" />
            </>
        );
}

export default Login;