import React from 'react';
import Table from 'react-bootstrap/Table';
import profilePic from './assets/pp.jpg';
import { useHistory } from "react-router-dom";
import running from './assets/running.gif';

function Home() {

    const history = useHistory();

    const routeChange = () => {
        let path = '/';
        history.push(path);
    }

    const routeChange2 = () => {
        let path = '/script-management';
        history.push(path);
    }

    return(
        <>
        <nav className="navbar navbar-expand-lg navbar-light fixed-top">
        <div className="container">
          <div className="collapse navbar-collapse" id="navbarTogglerDemo02">
            <ul className="navbar-nav ml-auto">
            <li className="nav-item">
                <button className="btn btn-primary" onClick={routeChange2}>Gestió de scripts</button>
              </li>
            <li className="nav-item">
                <button className="btn btn-primary" onClick={routeChange}>Logout</button>
              </li>
            </ul>
          </div>
        </div>
      </nav>
        <Table striped bordered hover size="sm">
            {/* M'ha fet mandra posar els cognoms */}
            <thead>
                <tr>
                    <th>#</th>
                    <th>Nom i Cognom</th>
                    <th>Actitud</th>
                    <th>AC</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><img src={profilePic} alt="" /></td>
                    <td>Marc</td>
                    <td>A</td>
                    <td>8</td>
                </tr>
                <tr>
                    <td><img src={profilePic} alt="" /></td>
                    <td>Jacobo</td>
                    <td>A</td>
                    <td>9</td>
                </tr>
                <tr>
                    <td><img src={profilePic} alt="" /></td>
                    <td>David</td>
                    <td>A</td>
                    <td>10</td>
                </tr>
                <tr>
                    <td><img src={profilePic} alt="" /></td>
                    <td>Sergio</td>
                    <td>A</td>
                    <td>7</td>
                </tr>
                <tr>
                    <td><img src={profilePic} alt="" /></td>
                    <td>Toni</td>
                    <td>A</td>
                    <td>6.5</td>
                </tr>
                <tr>
                    <td><img src={profilePic} alt="" /></td>
                    <td>El que duerme</td>
                    <td>N</td>
                    <td>3</td>
                </tr>
                <tr>
                    <td><img src={profilePic} alt="" /></td>
                    <td>El de primera fila</td>
                    <td>A</td>
                    <td>10</td>
                </tr>
                <tr>
                    <td><img src={profilePic} alt="" /></td>
                    <td>Darío</td>
                    <td>N</td>
                    <td>4</td>
                </tr>
                <tr>
                    <td><img src={profilePic} alt="" /></td>
                    <td>Carles</td>
                    <td>A</td>
                    <td>8</td>
                </tr>
                <tr>
                    <td><img src={profilePic} alt="" /></td>
                    <td>El twittero</td>
                    <td>A</td>
                    <td>5</td>
                </tr>
                <tr>
                    <td><img src={profilePic} alt="" /></td>
                    <td>Arnau</td>
                    <td>A</td>
                    <td>9</td>
                </tr>
                <tr>
                    <td><img src={profilePic} alt="" /></td>
                    <td>Eric</td>
                    <td>A</td>
                    <td>8</td>
                </tr>
                <tr>
                    <td><img src={profilePic} alt="" /></td>
                    <td>Mar</td>
                    <td>A</td>
                    <td>7</td>
                </tr>
                <tr>
                    <td><img src={profilePic} alt="" /></td>
                    <td>Alex</td>
                    <td>A</td>
                    <td>9</td>
                </tr>
                <tr>
                    <td><img src={profilePic} alt="" /></td>
                    <td>Pedro</td>
                    <td>N</td>
                    <td>0</td>
                </tr>
                <tr>
                    <td><img src={profilePic} alt="" /></td>
                    <td>Gabriele</td>
                    <td>N</td>
                    <td>4.99</td>
                </tr>
                <tr>
                    <td><img src={profilePic} alt="" /></td>
                    <td>Jenny</td>
                    <td>A</td>
                    <td>10</td>
                </tr>
                <tr>
                    <td><img src={profilePic} alt="" /></td>
                    <td>Alba</td>
                    <td>A</td>
                    <td>6</td>
                </tr>
                <tr>
                    <td><img src={profilePic} alt="" /></td>
                    <td>Roger</td>
                    <td>A</td>
                    <td>5</td>
                </tr>
                <tr>
                    <td><img src={profilePic} alt="" /></td>
                    <td>El nº 20</td>
                    <td>A</td>
                    <td>7.5</td>
                </tr>
            </tbody>
        </Table>
        </>
    )
}

export default Home;