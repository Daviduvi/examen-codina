import React from "react";
import Table from "react-bootstrap/Table";
import { useHistory } from "react-router-dom";


function ScriptManagement() {

    const history = useHistory();

    const routeChange = () => {
        let path = '/';
        history.push(path);
    }

    const routeChange2 = () => {
        let path = '/home';
        history.push(path);
    }

    return(
        <>
        <nav className="navbar navbar-expand-lg navbar-light fixed-top">
        <div className="container">
          <div className="collapse navbar-collapse" id="navbarTogglerDemo02">
            <ul className="navbar-nav ml-auto">
            <li className="nav-item">
                <button className="btn btn-primary" onClick={routeChange2}>Home</button>
              </li>
            <li className="nav-item">
                <button className="btn btn-primary" onClick={routeChange}>Logout</button>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <h3>Gestió de scripts</h3>
      <Table striped bordered hover size="sm">
            <thead>
                <tr>
                    <th>Nom</th>
                    <th>Ruta</th>
                    <th>Accions</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Isaac</td>
                    <td>src/assets/isaac.png</td>
                    {/* posar un onclick dins al td per cridar la funcio d'esborrar (s'ha de fer)*/}
                    <td>Eliminar</td>
                </tr>
                <tr>
                    <td>Jacobo</td>
                    <td>A</td>
                    <td>Eliminar</td>
                </tr>
            </tbody>
        </Table>
    </>
    )
}

export default ScriptManagement;