import React, {useState} from "react";
import { useHistory } from "react-router-dom";

function Register() {

    const history = useHistory();

    const routeChange = () => {
        let path = '/';
        history.push(path);
    }

    let [email, setState] = useState();
    let [password, setPasswordState] = useState();
    let [code, setCodeState] = useState();

    let handleChange = (e) => {
        setState({email: e.target.value})
      }

    let handlePasswordChange = (e) => {
        setPasswordState({password: e.target.value})
    }

    let handleCodeChange = (e) => {
        setCodeState({code: e.target.value})
    }

        return (
            <form>
                <h3>Register</h3>

                <div className="form-group">
                    <label>Email</label>
                    <input type="email" className="form-control" onChange={handleChange} placeholder="Enter email" />
                </div>

                <div className="form-group">
                    <label>Password</label>
                    <input type="password" className="form-control" onChange={handlePasswordChange} placeholder="Enter password" />
                </div>

                <div className="form-group">
                    <label>Password</label>
                    <input type="password" className="form-control" onChange={handlePasswordChange} placeholder="Repeat password" />
                </div>

                <div className="form-group">
                    <label>Access code</label>
                    <input type="text" className="form-control" onChange={handleCodeChange} placeholder="Enter access code" />
                </div>

                <button type="submit" className="btn btn-dark btn-lg btn-block" disabled={!email || !password || !code} onClick={routeChange}>Register</button>
            </form>
        );
}

export default Register;